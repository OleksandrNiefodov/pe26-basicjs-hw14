/* Теоритичні питання:
1. В чому полягає відмінність localStorage і sessionStorage?

- localStorage зберігає дані в локальному сховищі браузера, тобто навіть якщо юзер закриє табу браузера, а потім знову відкриє, то все що підпадає під localStorage буде збережено
- sessionStorage зберігає дані в сесійному сховищі браузера, в цьому випадку зберігається тільки в разі якщо не закривати вкладку браузеру, а наприклад натиснути на кнопку оновити.

2. Які аспекти безпеки слід враховувати при збереженні чутливої інформації, такої як паролі, за допомогою localStorage чи sessionStorage?

- небезпека може бути наступна, якщо пароль ніяк не токенізується, або криптується і т.і., то хтось після вас може відкрити ту ж вкладку і мати доступ до ваших даних.


3. Що відбувається з даними, збереженими в sessionStorage, коли завершується сеанс браузера?

- Коли сеанс заершується, дані юзера видаляються, вони зберігаються в пам'яті браузера до моменту завершення сеансу.

Практичне завдання:

Реалізувати можливість зміни колірної теми користувача.

Технічні вимоги:

- Взяти готове домашнє завдання HW-4 "Price cards" з блоку Basic HMTL/CSS.
- Додати на макеті кнопку "Змінити тему".
- При натисканні на кнопку - змінювати колірну гаму сайту (кольори кнопок, фону тощо) на ваш розсуд. 
    При повторному натискання - повертати все як було спочатку - начебто для сторінки доступні дві колірні теми.
- Вибрана тема повинна зберігатися після перезавантаження сторінки.

Примітки: 
- при виконанні завдання перебирати та задавати стилі всім елементам за допомогою js буде вважатись помилкою;
- зміна інших стилів сторінки, окрім кольорів буде вважатись помилкою.

Додаткові матеріали: https://developer.mozilla.org/en-US/docs/Web/CSS/Using_CSS_custom_properties.
*/ 


const changeThemeBtn = document.querySelector('#change-theme-button');
const activeTheme = localStorage.getItem('activeTheme');
const root = document.documentElement; 

if (activeTheme === 'true') {
	changeThemeBtn.classList.add('active');
	root.style.setProperty('--bg-color', '#66FF96');
	root.style.setProperty('--bg-color-pro-card-price', '#84B88E');
    root.style.setProperty('--bg-color-theme-button', '#fff');
}

changeThemeBtn.addEventListener('click', (event) => {
	event.target.classList.toggle('active');

	if (event.target.classList.contains('active')) {
		root.style.setProperty('--bg-color', '#66FF96');
		root.style.setProperty('--bg-color-pro-card-price', '#84B88E');
        root.style.setProperty('--bg-color-theme-button', '#fff');
        changeThemeBtn.textContent = 'BOO! Get back'; 
	} else {
		root.style.setProperty('--bg-color', '#fff');
		root.style.setProperty('--bg-color-pro-card-price', '#161A34');
        root.style.setProperty('--bg-color-theme-button', '#66FF96');
        changeThemeBtn.textContent = 'Change theme'; 
	}

	localStorage.setItem('activeTheme', event.target.classList.contains('active'));
})
